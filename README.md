# Introduction 
This repository contains the Experience API for EIP/PSS Booking.

## Libraries
The RAML model depends on the OTA model.
The OTA model is managed in a separate repository *raml-lib-ota*

The OTA RAML library is imported into this repository as a git subtree.
Updates to the OTA library *must* be git subtree pull from the *raml-lib-ota*

### Git Setup
If you do not have a remote setup to *raml-lib-ota* you should do the following.

For example, at the root of your Git repository
  git remote add ac-ota https://XXX@bitbucket.org/eippss/raml-lib-ota.git
The above assumes you're using HTTPS to access Git.

Now to pull the subtree
 git subtree add --prefix=src/main/api/library/ota --squash ac-ota develop
The above assumes you're adding the subtree from develop in *raml-lib-ota*


## Git Update Subtree
If you need an updated copy of the OTA library do a subtree pull
 git subtree pull --prefix=src/main/api/library/ota --squash ac-ota develop
The above assumes you're retrieving the most recent changes from develop in *raml-lib-ota*
